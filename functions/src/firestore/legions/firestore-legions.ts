import { BaseFields, Firestore } from "../firestore";
import { CollectionReference } from "@google-cloud/firestore";
import { FirestorePersonasSnapshot } from "../personas/firestore-personas";
import { FirestoreGroupsSnapshot } from "../groups/firestore-groups";

export interface FirestoreLegionsFields extends BaseFields {
    personaId?: string,
    groupId?: string,
    personaSnapshot?: FirestorePersonasSnapshot,
    groupSnapshot?: FirestoreGroupsSnapshot,
};

export interface FiresotreLegionsSnapshot {
};

export class FirestoreLegions {

    public static getCollection = (): CollectionReference => Firestore.database.collection('legions');
};
