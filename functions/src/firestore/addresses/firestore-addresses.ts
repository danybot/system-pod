import { BaseFields, Firestore, GeoPoints } from "../firestore";
import { CollectionReference } from "@google-cloud/firestore";

export interface FirestoreAddressesFields extends BaseFields {
    addressOne?: string,
    addressTwo?: string,
    city?: string,
    state?: string,
    country?: string,
    postcode?: number,
    location?: GeoPoints,
    targetId?: string,
};

export interface FirestoreAddressesSnapshot {
    address?: string,
    city?: string,
    state?: string,
};

export class FirestoreAddresses {

    public static getCollection = (): CollectionReference => Firestore.database.collection('addresses');

    public static addAddress = async (data: FirestoreAddressesFields): Promise<FirestoreAddressesFields> => {
        return await Firestore.add(FirestoreAddresses.getCollection(), data);
    };

    public static getAddressByTargetId = async (targetId: string, all: boolean = false): Promise<FirestoreAddressesFields[]> => {
        const collection = FirestoreAddresses.getCollection()
            .where('targetId', '==', targetId);

        if (!all) collection.where('isDeleted', '==', false);

        return await Firestore.getByQuery<FirestoreAddressesFields>(collection)
    };
};
