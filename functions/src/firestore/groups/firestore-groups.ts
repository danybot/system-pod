import { BaseFields, Firestore } from "../firestore";
import { CollectionReference } from "@google-cloud/firestore";

export enum GROUP_TYPE {
    PUBLIC = 'public',
    PRIVATE = 'private',
    PROTECTED = 'protected',
};

export interface FirestoreGroupsFields extends BaseFields {
    name?: string,
    hint?: string,
    type?: GROUP_TYPE,
    systemId?: string,
};

export interface FirestoreGroupsSnapshot {
    name?: string,
    hint?: string,
    type?: GROUP_TYPE,
};

export class FirestoreGroups {

    public static getCollection = (): CollectionReference => Firestore.database.collection('groups');

};
