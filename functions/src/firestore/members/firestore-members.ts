import { BaseFields, Firestore, Timestamp } from "../firestore";
import { CollectionReference } from "@google-cloud/firestore";

export interface FirestoreMembersFields extends BaseFields {
    firstName?: string,
    lastName?: string,
    email?: string,
    phoneNumber?: string,
    dateOfBirth?: Timestamp,
};

export interface FirestoreMembersSnapshot {
    name?: string,
    email?: string,
};

export class FirestoreMembers {

    public static getCollection = (): CollectionReference => Firestore.database.collection('members');

    public static addMember = async (data: FirestoreMembersFields): Promise<FirestoreMembersFields> => {
        return await Firestore.add(FirestoreMembers.getCollection(), data);
    };

    public static getMemberByEmail = async (email: string, all: boolean = false): Promise<FirestoreMembersFields | null> => {
        const collection = FirestoreMembers.getCollection()
            .where('email', '==', email);
        
        if (!all) collection.where('isDeleted', '==', false);
        
        const respone: FirestoreMembersFields[] = await Firestore.getByQuery<FirestoreMembersFields>(collection)

        return (respone && respone.length > 0) ? respone[0] : null;
    };
};
