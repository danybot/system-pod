import { BaseFields, Firestore } from "../firestore";
import { FirestoreMembersSnapshot } from "../members/firestore-members";
import { CollectionReference } from "@google-cloud/firestore";

export enum PERSONA_TYPE {
    ADMIN = 'admin',
    USER = 'user',
}

// member + system
export interface FirestorePersonasFields extends BaseFields {
    memberId?: string,
    systemId?: string,
    type?: PERSONA_TYPE,
    memberSnapshot: FirestoreMembersSnapshot,

};

export interface FirestorePersonasSnapshot {

};

export class FirestorePersonas {

    public static getCollection = (): CollectionReference => Firestore.database.collection('personas')
};
