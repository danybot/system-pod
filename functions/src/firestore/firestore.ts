import * as admin from 'firebase-admin'
import { CollectionReference, DocumentReference, Query } from '@google-cloud/firestore';

admin.initializeApp()

export type Timestamp = admin.firestore.Timestamp;

export type GeoPoints = admin.firestore.GeoPoint;

export interface BaseFields {
    id?: string,
    _isDeleted?: boolean,
    _createdAt?: Timestamp,
    _deletedAt?: Timestamp,
    _modifiedAt?: Timestamp,
};

export class Firestore {

    public static database = admin.firestore();

    public static now = (): Timestamp => {
        return admin.firestore.Timestamp.now()
    };

    public static add = async <T extends BaseFields>(collectionRef: CollectionReference, data: T): Promise<T> => {
        // Pre Process
        data._createdAt = Firestore.now();
        data._deletedAt = Firestore.now();
        data._modifiedAt = Firestore.now();
        data._isDeleted = false;

        // Insert
        const documentRef: DocumentReference = collectionRef.doc();
        await documentRef.set(data);

        // Post Process
        return {
            ...data,
            id: documentRef.id,
        };
    };

    public static update = async <T extends BaseFields>(documentRef: DocumentReference, data: T): Promise<T> => {
        // Pre Process
        data._modifiedAt = Firestore.now();

        // Update
        await documentRef.update(data);

        return {
            ...data,
        };
    };

    public static delete = async (documentRef: DocumentReference): Promise<boolean> => {
        // Pre Process
        const data: BaseFields = {
            _isDeleted: true,
            _deletedAt: Firestore.now(),
        };

        // Update
        await Firestore.update(documentRef, data);

        return true;
    };

    public static getDocument = async <T extends BaseFields>(documentRef: DocumentReference): Promise<T> => {
        // Fetch 
        const response = await documentRef.get();

        // Add Id
        const data: T = response.data() as T;
        data.id = response.id;

        // Response
        return data
    };

    public static getByQuery = async <T extends BaseFields>(queryRef: Query): Promise<T[]> => {
        // Fetch
        const response = await queryRef.get()
        let collection: T[] = [];

        // Add Id
        for (const doc of response.docs) {
            const data = doc.data() as T;
            data.id = doc.id
            collection.push(data)
        }

        // Response
        return collection;
    };
};
