import { BaseFields, Firestore } from "../firestore";
import { CollectionReference } from "@google-cloud/firestore";

export interface FirestoreDevicesFields extends BaseFields {
    name?: string,
    frimware?: string,
    hardwareId?: string,
    hardwareName?: string,
};

export interface FirestoreDevicesSnapshot {
    name?: string,
    hardwareName?: string,
};

export class FirestoreDevices {
    
    public static getCollection = (): CollectionReference => Firestore.database.collection('devices');

}
