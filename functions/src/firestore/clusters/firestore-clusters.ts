import { BaseFields, Firestore } from "../firestore";
import { CollectionReference } from "@google-cloud/firestore";
import { FirestoreGroupsSnapshot } from "../groups/firestore-groups";
import { FirestoreDevicesSnapshot } from "../devices/firestore-devices";

export enum CLUSTER_TYPE {
    READ = 'read',
    READ_WRITE = 'read_write',
};

export interface FirestoreClustersFields extends BaseFields {
    groupId?: string,
    deviceId?: string,
    type?: CLUSTER_TYPE,
    groupSnapshot?: FirestoreGroupsSnapshot,
    deviceSnapshot?: FirestoreDevicesSnapshot,
};

export interface FirestoreClustersSnapshot {

};

export class FirestoreClusters {
    
    public static getCollection = (): CollectionReference  => Firestore.database.collection('clusters');

};
