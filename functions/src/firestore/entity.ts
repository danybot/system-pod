import { BaseFields, Timestamp, GeoPoints } from "./firestore";

export interface FirebaseMembersFields extends BaseFields {
    firstName?: string,
    lastName?: string,
    email?: string,
    phoneNumber?: string,
    dateOfBirth?: Timestamp,
};

export interface FirebaseMemebersSnapshot {
    name?: string,
    email?: string,
};

export interface FirebaseAddressesFields extends BaseFields {
    addressOne?: string,
    addressTwo?: string,
    city?: string,
    state?: string,
    country?: string,
    postcode?: number,
    location?: GeoPoints
    targetId?: string,
};

export interface FirebaseAddressesSnapshot {
    address?: string,
    city?: string,
    state?: string,
};

export interface FirebaseSystemsFields extends BaseFields {
    name?: string,
    hint?: string,
    type?: SYSTEM_TYPE,
};

export interface FirebaseSystemsSnapshot {
    name?: string,
    hint?: string,
    type?: SYSTEM_TYPE,
};

export interface FirebaseGroupsFields extends BaseFields {
    name?: string,
    hint?: string,
    type?: GROUP_TYPE,
    systemId?: string,
};

export interface FirebaseGroupsSnapshot {
    name?: string,
    hint?: string,
    type?: GROUP_TYPE,
};

export interface FirebaseDevicesFields extends BaseFields {
    name?: string,
    frimware?: string,
    hardwareId?: string,
    hardwareName?: string,
};

export interface FirebaseDevicesSnapshot {
    name?: string,
    hardwareName?: string,
};

// member - system -> Persona
export interface FirebasePersonasFields extends BaseFields {
    memberId?: string,
    systemId?: string,
    type?: PERSONA_TYPE,
    memberSnapshot?: FirebaseMemebersSnapshot,
    systemSnapshot?: Map<string, string>,
};

// group - device -> Cluster
export interface FirebaseClusterFields extends BaseFields {
    groupId?: string,
    deviceId?: string,
    type?: CLUSTER_TYPE,
    groupSnapshot?: Map<string, string>,
    deviceSnapshot?: Map<string, string>
};

// persona - group -> Clan
export interface FirebaseClanFields extends BaseFields {
    personaId?: string,
    groupId?: string,
    personaSnapshot?: Map<string, string>,
    groupSnapshot?: Map<string, string>,
};

 


export enum CLUSTER_TYPE {
    READ = 'read',
    READ_WRITE = 'read_write',
};

export enum PERSONA_TYPE {
    ADMIN = 'admin',
    USER = 'user',
};

export enum SYSTEM_TYPE {
    HOUSE = 'house',
    OFFICE = 'office',
};

export enum GROUP_TYPE {
    PUBLIC = 'public',
    PRIVATE = 'private',
    PROTECTED = 'protected',
};
