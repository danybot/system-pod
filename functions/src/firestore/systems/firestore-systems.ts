import { BaseFields, Firestore } from "../firestore";
import { CollectionReference } from "@google-cloud/firestore";

export enum SYSTEM_TYPE {
    HOUSE = 'house',
    OFFICE = 'office',
};

export interface FirestoreSytemsField extends BaseFields {
    name?: string,
    hint?: string,
    type?: SYSTEM_TYPE,
};

export interface FirestoreSystemsSnapshot {
    name?: string,
    hint?: string,
    type?: SYSTEM_TYPE,
};

export class FirestoreSystems {

    public static getCollection = (): CollectionReference => Firestore.database.collection('systems');
};
