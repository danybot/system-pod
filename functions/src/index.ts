import * as functions from 'firebase-functions';
import * as HttpStatus from 'http-status-codes'
import { FirestoreMember } from './firestore/members/firebase-members';


export const poke = functions.https.onRequest(async (request, response) => {
    const status = {
        isAlive: true,
    }

    const memeber = await FirestoreMember.getMemberByEmail('brickheck@gmail.com');

    response.status(HttpStatus.OK).send({ ...status, ...memeber })
});